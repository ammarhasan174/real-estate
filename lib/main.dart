import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:realestate/screens/itemScreen.dart';

import 'constants.dart';

void main() async {
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
    systemNavigationBarColor: HexColor("EEEEEE"), // navigation bar color
    statusBarColor: HexColor("EEEEEE"),
        statusBarIconBrightness: Brightness.dark
  ));
  WidgetsFlutterBinding.ensureInitialized();
  const keyApplicationId = 'n7JbtvzH068A22OnKN4yAMob8RAKNhqhGTUrTJbf';
  const keyClientKey = 'XS3fFS4CeHOFrDgR3ns4QN4syZuwuM5CjlJFwfrQ';
  const keyParseServerUrl = 'https://parseapi.back4app.com';

  await Parse().initialize(keyApplicationId, keyParseServerUrl,
      clientKey: keyClientKey, autoSendSessionId: true);

  runApp(const MaterialApp(debugShowCheckedModeBanner: false, home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    width = screenSize.width;
    height = screenSize.height;
    final ds = width < 600 ? const Size(390, 844) : const Size(695, 844);
    return ScreenUtilInit(
      designSize: ds,
      minTextAdapt: false,
      splitScreenMode: true,
      builder: (context, child) {
        return GetMaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            textTheme: Typography.englishLike2018.apply(fontSizeFactor: 1.sp),
          ),
          home: child,
        );
      },
      child: SafeArea(child: itemScreen()),
    );
  }
}
