import 'package:auto_direction/auto_direction.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:realestate/constants.dart';
import 'package:realestate/controllers/itemsController.dart';
import 'package:realestate/controllers/pagerController.dart';
import 'package:realestate/models/estateModel.dart';
import 'package:realestate/screens/detailsScreen.dart';
import 'package:realestate/screens/favScreen.dart';

class itemScreen extends StatelessWidget {
  itemScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isMobile = width >= 600 ? false : true;
    List<String> cityList = [
      "الكل",
      "دمشق",
      "طرطوس",
      "درعا",
      "السويداء",
      "حمص",
      "ريف دمشق",
      "اللاذقية",
      "حماة",
      "إدلب",
      "حلب",
      "الرقة",
      "الحسكة",
      "القنيطرة",
      "دير الزور"
    ];
    List<String> optionList = ["الكل", "للبيع", "للأجار"];

    return Stack(
      children: [
        Scaffold(
          floatingActionButtonLocation: isMobile
              ? FloatingActionButtonLocation.startFloat
              : FloatingActionButtonLocation.centerFloat,
          floatingActionButton: GetBuilder<itemsController>(builder: (c) {
            return c.favoriteList.isNotEmpty
                ? AnimatedScale(
                    duration: const Duration(milliseconds: 250),
                    scale: c.showSearchBar ? 1 : 0,
                    alignment: Alignment.center,
                    child: Container(
                      margin: EdgeInsets.only(
                          bottom: 10.h, left: isMobile ? 10.w : 0),
                      child: RawMaterialButton(
                        fillColor: Colors.white,
                        shape: CircleBorder(),
                        onPressed: () {
                          Get.to(() => favScreen());
                        },
                        elevation: 6,
                        splashColor: Colors.red.withOpacity(0.3),
                        hoverColor: Colors.red.withOpacity(0.3),
                        padding: EdgeInsets.all(15.sp),
                        child: Icon(
                          Icons.favorite,
                          size: 30.sp,
                          color: Colors.red,
                        ),
                      ),
                    ),
                  )
                : Container();
          }),
          body: Container(
            color: HexColor("EEEEEE"),
            child: GetBuilder<itemsController>(
                init: itemsController(),
                builder: (c) {
                  List<estateModel> mList = [];
                  // if (c.isSearching) {
                  //   mList = c.tempList;
                  // } else {
                  //   mList = c.displayList;
                  // }
                  mList = c.displayList;
                  return Stack(
                    alignment: Alignment.topCenter,
                    children: [
                      NotificationListener<UserScrollNotification>(
                        onNotification: (dir) {
                          if (dir.direction == ScrollDirection.forward) {
                            c.showSearchBar = true;
                            c.update();
                          } else if (dir.direction == ScrollDirection.reverse) {
                            c.showSearchBar = false;
                            c.update();
                          }
                          return true;
                        },
                        child: c.displayList.isNotEmpty
                            ? width < 600
                                ? ListView.separated(
                                    keyboardDismissBehavior:
                                        ScrollViewKeyboardDismissBehavior
                                            .onDrag,
                                    padding: EdgeInsets.only(top: 110.h),
                                    // padding: EdgeInsets.only(top: 120.h),
                                    physics: const BouncingScrollPhysics(),
                                    itemBuilder: (context, i) =>
                                        GestureDetector(
                                      onTap: () {
                                        Get.to(() =>
                                            detailsScreen(model: mList[i]));
                                      },
                                      child: itemViewMobile(
                                          context: context,
                                          model: mList[i],
                                          index: i),
                                    ),
                                    itemCount: mList.length,
                                    separatorBuilder: (c, i) => SizedBox(
                                      height: 20.h,
                                    ),
                                  )
                                : GridView.builder(
                                    padding: EdgeInsets.only(top: 110.h),
                                    // padding: EdgeInsets.only(top: 115.h),
                                    physics: const BouncingScrollPhysics(),
                                    gridDelegate:
                                        const SliverGridDelegateWithMaxCrossAxisExtent(
                                      maxCrossAxisExtent: 550,
                                      childAspectRatio: 1,
                                    ),
                                    itemCount: mList.length,
                                    itemBuilder: (c, i) => GestureDetector(
                                          onTap: () {
                                            Get.to(() =>
                                                detailsScreen(model: mList[i]));
                                          },
                                          child: itemViewTablet(
                                              context: context,
                                              model: mList[i]),
                                        ))
                            : Center(
                                child: (!c.isSearching && c.noResult)
                                    ? Text(
                                        "لايوجد نتائج",
                                        style: TextStyle(
                                            fontSize: 20.sp,
                                            fontStyle: FontStyle.italic),
                                      )
                                    : const CircularProgressIndicator(),
                              ),
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          AnimatedCrossFade(
                            secondCurve: Curves.fastOutSlowIn,
                            duration: const Duration(milliseconds: 300),
                            crossFadeState: c.showSearchBar
                                ? CrossFadeState.showFirst
                                : CrossFadeState.showSecond,
                            secondChild: Container(),
                            firstChild: Column(
                              children: [
                                Container(
                                  color: HexColor("EEEEEE"),
                                  padding: EdgeInsets.
                                  only(right: 20.w,left: 20.w,top: 15.h,bottom: 8.h),
                                  child: SizedBox(
                                    height: 45.h,
                                    child: Row(
                                      children: [
                                        AutoDirection(
                                          text: c.searchCont.text,
                                          child: Flexible(
                                            child: TextFormField(
                                              focusNode: c.searchFoc,
                                              controller: c.searchCont,
                                              cursorColor: HexColor("0D4599"),
                                              textAlignVertical:
                                                  TextAlignVertical.center,
                                              maxLines: 1,
                                              onTap: () {
                                                c.searchCont.selection =
                                                    TextSelection.collapsed(
                                                        offset: c.searchCont.text
                                                            .length);
                                              },
                                              onChanged: (v) {
                                                c.filterList();
                                              },
                                              // textDirection: TextDirection.rtl,
                                              style: TextStyle(
                                                  fontSize: 18.sp,
                                                  color: HexColor("707070")),
                                              decoration: InputDecoration(
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10.w),
                                                  border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              18.r),
                                                      borderSide: BorderSide.none),
                                                  fillColor: Colors.white,
                                                  filled: true,
                                                  hintText: "بحث",
                                                  hintTextDirection:
                                                      TextDirection.rtl,
                                                  hintStyle: TextStyle(
                                                      fontSize: 18.sp,
                                                      color: HexColor("707070"))),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 10.w),
                                          child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: c.searchCont.text.isEmpty
                                                ? Icon(Icons.search,
                                                    color: HexColor("707070"))
                                                : IconButton(
                                                    onPressed: () {
                                                      c.searchCont.clear();
                                                      // c.searchFunction(str: "");
                                                      c.filterList();
                                                      c.searchFoc.unfocus();
                                                      c.update();
                                                    },
                                                    icon: Icon(
                                                      Icons.clear,
                                                      color: HexColor("0D4599"),
                                                    )),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                    height: 43.h,
                                    color: HexColor("EEEEEE"),
                                    width: double.infinity,
                                    padding: EdgeInsets.only(bottom: 6.h),
                                    child: Center(
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        reverse: true,
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder: (c, i) =>
                                            optionsListFilter(option: optionList[i]),
                                        itemCount: optionList.length,
                                      ),
                                    ))
                              ],
                            ),
                          ),

                        ],
                      ),
                    ],
                  );
                }),
          ),
        ),
        GetX<itemsController>(builder: (c) {
          return c.showLoading_load.value
              ? SizedBox.expand(
                  child: Container(
                      color: Colors.white.withOpacity(0.8),
                      child: const Center(child: CircularProgressIndicator())),
                )
              : const SizedBox();
        })
      ],
    );
  }

  Widget optionsListFilter({required String option}) {
    return GetX<itemsController>(builder: (c) {
      bool isChecked = c.option_checked.value == option;
      return GestureDetector(
        onTap: () {
          c.searchFoc.unfocus();
          c.option_checked.value = option;
          c.filterList();
        },
        child: AnimatedContainer(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          margin: EdgeInsets.only(right: 10.w),
          decoration: BoxDecoration(
            color: isChecked
                ? HexColor("0D4599").withOpacity(0.7)
                : HexColor("DEDEDE"),
            borderRadius: BorderRadius.circular(1000),
          ),
          curve: Curves.easeInOutQuad,
          duration: const Duration(milliseconds: 250),
          child: Center(
            child: Text(
              option,
              style: TextStyle(
                fontSize: 16.sp,
                color: isChecked ? Colors.white : HexColor("5A5A5A"),
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
        ),
      );
    });
  }

  Widget itemViewTablet(
      {required BuildContext context, required estateModel model}) {
    final titleStyle = TextStyle(
        overflow: TextOverflow.ellipsis,
        fontFamily: "assets/fonts/jost_light.tff",
        color: HexColor("0D4599"),
        fontSize: 20.sp);
    final locationStyle = TextStyle(
        fontFamily: "assets/fonts/jost_light.tff",
        color: Colors.white,
        fontSize: 14.sp);

    final descStyle = TextStyle(
      fontFamily: "assets/fonts/jost_light.tff",
      color: HexColor("FEFEFE"),
      fontSize: 16.sp,
    );
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: HexColor("0D4599").withAlpha(60),
            offset: Offset(0, 4.h),
            blurRadius: 5.r,
          ),
          model.isSpecial
              ? BoxShadow(
                  color: HexColor("FFD700"),
                  spreadRadius: 1,
                  offset: Offset(0, 0.h),
                  blurRadius: 10.r,
                )
              : const BoxShadow()
        ],
        borderRadius: BorderRadius.circular(17.r),
      ),
      margin: EdgeInsets.symmetric(horizontal: 8.w, vertical: 8.h),
      child: Stack(
        fit: StackFit.expand,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(17.r),
            child: CachedNetworkImage(
              imageUrl: model.imageUrlsList[0],
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              progressIndicatorBuilder: (context, url, downloadProgress) =>
                  Center(
                      child: CircularProgressIndicator(
                          value: downloadProgress.progress,
                          color: Colors.black12)),
              errorWidget: (context, url, error) => Icon(
                Icons.error,
                color: Colors.red,
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(17.r),
                gradient: LinearGradient(
                    end: Alignment.topCenter,
                    begin: Alignment.bottomCenter,
                    colors: [
                      HexColor("162E53"),
                      Colors.transparent,
                      Colors.transparent,
                    ])),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.only(top: 22.h, right: 13.w, left: 13.w),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 10,
                      child: Container(
                        padding:
                        EdgeInsets.symmetric(horizontal: 14.w, vertical: 2.h),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(11.r),
                            color: HexColor("EDF1FD")),
                        child: Text(
                          model.state,
                          style: TextStyle(
                            fontFamily: "assets/fonts/bungee_hairline.ttf",
                            color: HexColor("0D4599"),
                            fontSize: 14.sp,
                          ),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 40,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "${model.updatedAt.year}-${model.updatedAt.month}-${model.updatedAt.day}",
                            style: locationStyle,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                child: Text(
                                  "${model.city} - ${model.location}",
                                  style: locationStyle,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  textAlign: TextAlign.end,
                                ),
                              ),
                              // Flexible(
                              //   child: Text(
                              //     " - ",
                              //     style: locationStyle,
                              //   ),
                              // ),
                              // Flexible(
                              //   child: Text(
                              //     model.city,
                              //     style: locationStyle,
                              //   ),
                              // ),
                              // Flexible(
                              //   child: SizedBox(
                              //     width: 4.w,
                              //   ),
                              // ),
                              Icon(
                                Icons.location_on_rounded,
                                color: HexColor("0D4599"),
                                size: 22.sp,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.w),
                    padding:
                        EdgeInsets.symmetric(horizontal: 13.w,),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.r),
                        color: Colors.white),
                    child: Text(
                      model.name,
                      style: titleStyle,
                      maxLines: 1,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: 9.h, left: 8.w, right: 8.w, bottom: 6.h),
                    child: Text(
                      model.desc,
                      style: descStyle,
                      textAlign: TextAlign.end,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 28.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      textDirection: TextDirection.rtl,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          textDirection: TextDirection.rtl,
                          children: [
                            Icon(
                              Icons.square_foot_outlined,
                              color: HexColor("CACACA"),
                              size: 22.sp,
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Text(
                              model.space,
                              style: TextStyle(
                                  overflow: TextOverflow.ellipsis,
                                  fontFamily: "assets/fonts/jost_light.ttf",
                                  fontSize: 18.sp,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: [
                            Icon(
                              Icons.bed_outlined,
                              color: HexColor("CACACA"),
                              size: 22.sp,
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Text(
                              model.rooms_count,
                              style: TextStyle(
                                  fontFamily: "assets/fonts/jost_light.ttf",
                                  fontSize: 18.sp,
                                  color: Colors.white),
                            )
                          ],
                        ),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: [
                            Icon(
                              Icons.bathroom_outlined,
                              color: HexColor("CACACA"),
                              size: 22.sp,
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Text(
                              model.baths_count,
                              style: TextStyle(
                                  fontFamily: "assets/fonts/jost_light.ttf",
                                  fontSize: 18.sp,
                                  color: Colors.white),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget itemViewMobile(
      {required BuildContext context,
      required estateModel model,
      required int index}) {
    const int imageFlex = 40;
    // final int imageFlex=width<450?35:width<500?30:width<550?25:width<600?20:35;
    const int textFlex = 50;
    final titleStyle = TextStyle(
        overflow: TextOverflow.ellipsis,
        fontFamily: "assets/fonts/bungee_hairline.ttf",
        color: HexColor("0D4599"),
        fontSize: 20.sp);
    final locationStyle = TextStyle(
      fontFamily: "assets/fonts/jost_light.ttf",
      color: HexColor("405468"),
      fontSize: 14.sp,
    );
    final descStyle = TextStyle(
      fontFamily: "assets/fonts/jost_light.ttf",
      color: HexColor("777777").withOpacity(0.6),
      fontSize: 13.sp,
    );

    return Container(
      width: double.infinity,
      // height: 136.h,
      // height: double.,
      // margin: EdgeInsets.symmetric(horizontal: 9.w),
      margin: EdgeInsets.only(left: 9.w, right: 9.w, top: index == 0 ? 2 : 0),

      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.r),
        boxShadow: [
          BoxShadow(
            color: HexColor("0D4599").withAlpha(60),
            offset: Offset(0, 4.h),
            blurRadius: 5.r,
          ),
          model.isSpecial
              ? BoxShadow(
                  color: HexColor("FFD700"),
                  spreadRadius: 1,
                  offset: Offset(0, 0.h),
                  blurRadius: 0.r,
                )
              : const BoxShadow()
        ],
      ),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
              flex: textFlex,
              child: Padding(
                padding: EdgeInsets.only(top: 8.h, left: 10.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.r),
                              color: HexColor("EDF1FD"),
                            ),
                            padding: EdgeInsets.symmetric(
                                horizontal: 10.w, vertical: 5.h),
                            child: Text(
                              model.state,
                              style: TextStyle(
                                  overflow: TextOverflow.ellipsis,
                                  fontFamily:
                                      "assets/fonts/bungee_hairline.ttf",
                                  color: HexColor("0D4599"),
                                  fontSize: 12.sp),
                            )),
                        Expanded(
                          child: Text(
                            textDirection: TextDirection.rtl,
                            model.name,
                            style: titleStyle,
                            maxLines: 1,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Text(
                            model.location,
                            style: locationStyle,
                            maxLines: 1,
                            textAlign: TextAlign.end,
                          ),
                        ),
                        Text(
                          " - ",
                          style: locationStyle,
                        ),
                        Text(
                          model.city,
                          style: locationStyle,
                        ),
                        SizedBox(
                          width: 4.w,
                        ),
                        Icon(
                          Icons.location_on_rounded,
                          color: HexColor("0D4599"),
                          size: 22.sp,
                        )
                      ],
                    ),
                    Text(
                      model.desc,
                      style: descStyle,
                      textAlign: TextAlign.end,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10.w, vertical: 5.h),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        textDirection: TextDirection.rtl,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            textDirection: TextDirection.rtl,
                            children: [
                              Icon(
                                Icons.square_foot_outlined,
                                color: HexColor("9B9B9B"),
                                size: 22.sp,
                              ),
                              SizedBox(
                                width: 2.w,
                              ),
                              Text(
                                model.space,
                                style: TextStyle(
                                    overflow: TextOverflow.ellipsis,
                                    fontFamily: "assets/fonts/jost_light.ttf",
                                    fontSize: 14.sp,
                                    color: HexColor("5C5C5C")),
                              ),
                            ],
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: [
                              Icon(
                                Icons.bed_outlined,
                                color: HexColor("9B9B9B"),
                                size: 22.sp,
                              ),
                              SizedBox(
                                width: 2.w,
                              ),
                              Text(
                                model.rooms_count,
                                style: TextStyle(
                                    fontFamily: "assets/fonts/jost_light.ttf",
                                    fontSize: 14.sp,
                                    color: HexColor("5C5C5C")),
                              )
                            ],
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: [
                              Icon(
                                Icons.bathroom_outlined,
                                color: HexColor("9B9B9B"),
                                size: 22.sp,
                              ),
                              SizedBox(
                                width: 2.w,
                              ),
                              Text(
                                model.baths_count,
                                style: TextStyle(
                                    fontFamily: "assets/fonts/jost_light.ttf",
                                    fontSize: 14.sp,
                                    color: HexColor("5C5C5C")),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )),
          Expanded(
              flex: imageFlex,
              child: Padding(
                padding: EdgeInsets.only(
                    right: 5.w, left: 8.w, top: 5.h, bottom: 5.h),
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    AspectRatio(
                        aspectRatio: 4 / 3,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(18.r),
                          child: CachedNetworkImage(
                            imageUrl: model.imageUrlsList[0],
                            imageBuilder: (context, imageProvider) => Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            progressIndicatorBuilder:
                                (context, url, downloadProgress) => Center(
                                    child: CircularProgressIndicator(
                                        value: downloadProgress.progress,
                                        color: Colors.black12)),
                            errorWidget: (context, url, error) => Icon(
                              Icons.error,
                              color: Colors.red,
                            ),
                          ),
                        )),
                    Text(
                      "${model.updatedAt.year}-${model.updatedAt.month}-${model.updatedAt.day}",
                      style: TextStyle(
                          color: Colors.white.withOpacity(0.8),
                          fontSize: 13.sp),
                    )
                  ],
                ),
              )),
        ],
      ),
    );
  }
}
