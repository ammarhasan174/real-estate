import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:realestate/constants.dart';
import 'package:realestate/controllers/itemsController.dart';
import 'package:realestate/controllers/pagerController.dart';
import 'package:realestate/models/estateModel.dart';
import 'package:realestate/screens/detailsScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class favScreen extends StatelessWidget {
  favScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: HexColor("EEEEEE"),
          child: Stack(
            children: [
              GetBuilder<itemsController>(builder: (c) {
                List mList = c.favoriteList.value;

                return mList.isNotEmpty
                    ? width < 600
                        ? ListView.separated(
                            keyboardDismissBehavior:
                                ScrollViewKeyboardDismissBehavior.onDrag,
                            padding: EdgeInsets.only(top: 10.h),
                            physics: const BouncingScrollPhysics(),
                            itemBuilder: (context, i) => GestureDetector(
                              onTap: () {
                                Get.to(() => detailsScreen(model: mList[i]));
                              },
                              child: itemViewMobile(
                                  context: context, model: mList[i],index: i),
                            ),
                            itemCount: mList.length,
                            separatorBuilder: (c, i) => SizedBox(
                              height: 20.h,
                            ),
                          )
                        : GridView.builder(
                            padding: EdgeInsets.only(top: 10.h),
                            physics: const BouncingScrollPhysics(),
                            gridDelegate:
                                const SliverGridDelegateWithMaxCrossAxisExtent(
                              maxCrossAxisExtent: 550,
                              childAspectRatio: 1,
                            ),
                            itemCount: mList.length,
                            itemBuilder: (c, i) => GestureDetector(
                                  onTap: () {
                                    Get.to(
                                        () => detailsScreen(model: mList[i]));
                                  },
                                  child: itemViewTablet(
                                      context: context, model: mList[i]),
                                ))
                    : Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.heart_broken,
                                  color: HexColor("e97a7a"),
                                  size: 35.sp,
                                ),
                                SizedBox(
                                  width: 20.w,
                                ),
                                Text(
                                  "لايوجد عناصر مفضلة",
                                  style: TextStyle(
                                      fontSize: 25.sp, color: Colors.black),
                                ),
                              ],
                            ),
                            SizedBox(height: 30.h,),
                            TextButton(
                                onPressed: () {
                                  Get.back();
                                },
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(HexColor("F85454")),
                                  padding: MaterialStateProperty.all(EdgeInsets.zero)
                                      
                                ),
                                child: Text(
                                  "رجوع",
                                  style: TextStyle(
                                      fontSize: 18.sp, color: Colors.white),
                                ))
                          ],
                        ),
                      );
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget itemViewTablet(
      {required BuildContext context, required estateModel model}) {
    final titleStyle = TextStyle(
        overflow: TextOverflow.ellipsis,
        fontFamily: "assets/fonts/jost_light.tff",
        color: HexColor("0D4599"),
        fontSize: 20.sp);
    final locationStyle = TextStyle(
        fontFamily: "assets/fonts/jost_light.tff",
        color: Colors.white,
        fontSize: 14.sp);

    final descStyle = TextStyle(
      fontFamily: "assets/fonts/jost_light.tff",
      color: HexColor("FEFEFE"),
      fontSize: 16.sp,
    );
    return Container(
      decoration: BoxDecoration(
        boxShadow:  [
          BoxShadow(
            color: HexColor("0D4599").withAlpha(60),
            offset: Offset(0, 4.h),
            blurRadius: 5.r,
          ),

          model.isSpecial
              ? BoxShadow(
            color: HexColor("FFD700"),
            spreadRadius: 1,
            offset: Offset(0, 0.h),
            blurRadius: 10.r,
          )
              : const BoxShadow()

        ],
        borderRadius: BorderRadius.circular(17.r),
      ),
      margin: EdgeInsets.symmetric(horizontal: 8.w, vertical: 8.h),
      child: Stack(
        fit: StackFit.expand,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(17.r),
            child: CachedNetworkImage(
              imageUrl: model.imageUrlsList[0],
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              progressIndicatorBuilder: (context, url, downloadProgress) =>
                  Center(
                      child: CircularProgressIndicator(
                          value: downloadProgress.progress,
                          color: Colors.black12)),
              errorWidget: (context, url, error) => Icon(
                Icons.error,
                color: Colors.red,
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(17.r),
                gradient: LinearGradient(
                    end: Alignment.topCenter,
                    begin: Alignment.bottomCenter,
                    colors: [
                      HexColor("162E53"),
                      Colors.transparent,
                      Colors.transparent,
                    ])),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.only(top: 22.h, right: 13.w, left: 13.w),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 10,
                      child: Container(
                        padding:
                        EdgeInsets.symmetric(horizontal: 14.w, vertical: 2.h),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(11.r),
                            color: HexColor("EDF1FD")),
                        child: Text(
                          model.state,
                          style: TextStyle(
                            fontFamily: "assets/fonts/bungee_hairline.ttf",
                            color: HexColor("0D4599"),
                            fontSize: 14.sp,
                          ),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 40,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "${model.updatedAt.year}-${model.updatedAt.month}-${model.updatedAt.day}",
                            style: locationStyle,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                child: Text(
                                  "${model.city} - ${model.location}",
                                  style: locationStyle,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  textAlign: TextAlign.end,
                                ),
                              ),
                              // Flexible(
                              //   child: Text(
                              //     " - ",
                              //     style: locationStyle,
                              //   ),
                              // ),
                              // Flexible(
                              //   child: Text(
                              //     model.city,
                              //     style: locationStyle,
                              //   ),
                              // ),
                              // Flexible(
                              //   child: SizedBox(
                              //     width: 4.w,
                              //   ),
                              // ),
                              Icon(
                                Icons.location_on_rounded,
                                color: HexColor("0D4599"),
                                size: 22.sp,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.w),
                    padding: EdgeInsets.symmetric(
                        horizontal: 13.w),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.r),
                        color: Colors.white),
                    child: Text(
                      model.name,
                      style: titleStyle,
                      maxLines: 1,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: 9.h, left: 8.w, right: 8.w, bottom: 6.h),
                    child: Text(
                      model.desc,
                      style: descStyle,
                      textAlign: TextAlign.end,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 28.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      textDirection: TextDirection.rtl,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          textDirection: TextDirection.rtl,
                          children: [
                            Icon(
                              Icons.square_foot_outlined,
                              color: HexColor("CACACA"),
                              size: 22.sp,
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Text(
                              model.space,
                              style: TextStyle(
                                  overflow: TextOverflow.ellipsis,
                                  fontFamily: "assets/fonts/jost_light.ttf",
                                  fontSize: 18.sp,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: [
                            Icon(
                              Icons.bed_outlined,
                              color: HexColor("CACACA"),
                              size: 22.sp,
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Text(
                              model.rooms_count,
                              style: TextStyle(
                                  fontFamily: "assets/fonts/jost_light.ttf",
                                  fontSize: 18.sp,
                                  color: Colors.white),
                            )
                          ],
                        ),
                        Row(
                          textDirection: TextDirection.rtl,
                          children: [
                            Icon(
                              Icons.bathroom_outlined,
                              color: HexColor("CACACA"),
                              size: 22.sp,
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Text(
                              model.baths_count,
                              style: TextStyle(
                                  fontFamily: "assets/fonts/jost_light.ttf",
                                  fontSize: 18.sp,
                                  color: Colors.white),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget itemViewMobile(
      {required BuildContext context, required estateModel model,required int index}) {
    const int imageFlex = 40;
    // final int imageFlex=width<450?35:width<500?30:width<550?25:width<600?20:35;
    const int textFlex = 50;
    final titleStyle = TextStyle(
        overflow: TextOverflow.ellipsis,
        fontFamily: "assets/fonts/bungee_hairline.ttf",
        color: HexColor("0D4599"),
        fontSize: 20.sp);
    final locationStyle = TextStyle(
      fontFamily: "assets/fonts/jost_light.ttf",
      color: HexColor("405468"),
      fontSize: 14.sp,
    );
    final descStyle = TextStyle(
      fontFamily: "assets/fonts/jost_light.ttf",
      color: HexColor("777777").withOpacity(0.6),
      fontSize: 13.sp,
    );

    return Container(
      width: double.infinity,
      // height: 136.h,
      // height: double.,
      // margin: EdgeInsets.symmetric(horizontal: 9.w),
      margin: EdgeInsets.only(left: 9.w, right: 9.w, top: index == 0 ? 2 : 0),

      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.r),
        boxShadow: [
          BoxShadow(
            color: HexColor("0D4599").withAlpha(60),
            offset: Offset(0, 4.h),
            blurRadius: 5.r,
          ),
          model.isSpecial
              ? BoxShadow(
            color: HexColor("FFD700"),
            spreadRadius: 1,
            offset: Offset(0, 0.h),
            blurRadius: 0.r,
          )
              : const BoxShadow()
        ],
      ),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
              flex: textFlex,
              child: Padding(
                padding: EdgeInsets.only(top: 8.h, left: 10.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.r),
                              color: HexColor("EDF1FD"),
                            ),
                            padding: EdgeInsets.symmetric(
                                horizontal: 10.w, vertical: 5.h),
                            child: Text(
                              model.state,
                              style: TextStyle(
                                  overflow: TextOverflow.ellipsis,
                                  fontFamily:
                                  "assets/fonts/bungee_hairline.ttf",
                                  color: HexColor("0D4599"),
                                  fontSize: 12.sp),
                            )),
                        Expanded(
                          child: Text(
                            textDirection: TextDirection.rtl,
                            model.name,
                            style: titleStyle,
                            maxLines: 1,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Text(
                            model.location,
                            style: locationStyle,
                            maxLines: 1,
                            textAlign: TextAlign.end,
                          ),
                        ),
                        Text(
                          " - ",
                          style: locationStyle,
                        ),
                        Text(
                          model.city,
                          style: locationStyle,
                        ),
                        SizedBox(
                          width: 4.w,
                        ),
                        Icon(
                          Icons.location_on_rounded,
                          color: HexColor("0D4599"),
                          size: 22.sp,
                        )
                      ],
                    ),
                    Text(
                      model.desc,
                      style: descStyle,
                      textAlign: TextAlign.end,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    Padding(
                      padding:
                      EdgeInsets.symmetric(horizontal: 10.w, vertical: 5.h),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        textDirection: TextDirection.rtl,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            textDirection: TextDirection.rtl,
                            children: [
                              Icon(
                                Icons.square_foot_outlined,
                                color: HexColor("9B9B9B"),
                                size: 22.sp,
                              ),
                              SizedBox(
                                width: 2.w,
                              ),
                              Text(
                                model.space,
                                style: TextStyle(
                                    overflow: TextOverflow.ellipsis,
                                    fontFamily: "assets/fonts/jost_light.ttf",
                                    fontSize: 14.sp,
                                    color: HexColor("5C5C5C")),
                              ),
                            ],
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: [
                              Icon(
                                Icons.bed_outlined,
                                color: HexColor("9B9B9B"),
                                size: 22.sp,
                              ),
                              SizedBox(
                                width: 2.w,
                              ),
                              Text(
                                model.rooms_count,
                                style: TextStyle(
                                    fontFamily: "assets/fonts/jost_light.ttf",
                                    fontSize: 14.sp,
                                    color: HexColor("5C5C5C")),
                              )
                            ],
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: [
                              Icon(
                                Icons.bathroom_outlined,
                                color: HexColor("9B9B9B"),
                                size: 22.sp,
                              ),
                              SizedBox(
                                width: 2.w,
                              ),
                              Text(
                                model.baths_count,
                                style: TextStyle(
                                    fontFamily: "assets/fonts/jost_light.ttf",
                                    fontSize: 14.sp,
                                    color: HexColor("5C5C5C")),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )),
          Expanded(
              flex: imageFlex,
              child: Padding(
                padding: EdgeInsets.only(
                    right: 5.w, left: 8.w, top: 5.h, bottom: 5.h),
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    AspectRatio(
                        aspectRatio: 4 / 3,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(18.r),
                          child: CachedNetworkImage(
                            imageUrl: model.imageUrlsList[0],
                            imageBuilder: (context, imageProvider) => Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            progressIndicatorBuilder:
                                (context, url, downloadProgress) => Center(
                                child: CircularProgressIndicator(
                                    value: downloadProgress.progress,
                                    color: Colors.black12)),
                            errorWidget: (context, url, error) => Icon(
                              Icons.error,
                              color: Colors.red,
                            ),
                          ),
                        )),
                    Text("${model.updatedAt.year}-${model.updatedAt.month}-${model.updatedAt.day}",style: TextStyle(color: Colors.white.withOpacity(0.8),fontSize: 13.sp),)
                  ],
                ),
              )),

        ],
      ),
    );
  }
}
