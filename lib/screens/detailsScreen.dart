import 'package:blur/blur.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:draggable_home/draggable_home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:like_button/like_button.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:realestate/constants.dart';
import 'package:realestate/controllers/detailsController.dart';
import 'package:realestate/controllers/itemsController.dart';
import 'package:realestate/controllers/pagerController.dart';
import 'package:realestate/models/estateModel.dart';
import 'package:realestate/screens/favScreen.dart';
import 'package:telegram/telegram.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'dart:io' show Platform;

import 'package:zoom_pinch_overlay/zoom_pinch_overlay.dart';

class detailsScreen extends StatelessWidget {
  estateModel model;

  detailsScreen({required this.model});

  @override
  Widget build(BuildContext context) {
    var backgrounImage = CachedNetworkImage(
      imageUrl: model.imageUrlsList[0],
      fit: BoxFit.cover,
    );

    Get.put(detailsController());
    return SafeArea(
      child: Scaffold(
        body: width < 600
            ? bodyMobile_1(context: context, model: model, bg: backgrounImage)
            : bodyTablet(context: context, model: model, bg: backgrounImage),
      ),
    );
  }
}

Widget bodyTablet(
    {required BuildContext context,
    required estateModel model,
    required CachedNetworkImage bg}) {
  int currentIndex = 0;
  final locationStyle = TextStyle(
      fontFamily: "assets/fonts/jost_light.ttf",
      color: HexColor("405468"),
      fontSize: 16.sp);
  final attrStyle = TextStyle(
      fontFamily: "assets/fonts/jost_light.ttf",
      fontSize: 15.sp,
      color: HexColor("5C5C5C"));
  final commStyle = TextStyle(
      fontFamily: "assets/fonts/bungee_hairline.ttf",
      fontSize: 20.sp,
      color: HexColor("0D4599"));

  return DraggableHome(
    headerExpandedHeight: 0.5,
    stretchMaxHeight: 0.9,
    title: Container(),
    appBarColor: Colors.white,
    backgroundColor: Colors.white,
    headerWidget: Stack(
      // fit: StackFit.expand,
      // alignment: Alignment.center,
      children: [
        SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: FittedBox(
            fit: BoxFit.cover,
            child: Blur(
              blur: 7,
              child: bg,
            ),
          ),
        ),
        SizedBox.expand(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 35.h),
            child: GetBuilder<detailsController>(builder: (c) {
              return CarouselSlider(
                options: CarouselOptions(
                  scrollPhysics: const BouncingScrollPhysics(),
                  enlargeCenterPage: true,
                  enableInfiniteScroll: false,
                  onPageChanged: (i, r) {
                    currentIndex = i;
                    c.update();
                    print("current page is $currentIndex ($i)");
                  },
                  initialPage: currentIndex,
                ),
                items: model.imageUrlsList.map((i) {
                  return SizedBox.expand(
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(45.r),
                        child: CachedNetworkImage(
                          imageUrl: i,
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          progressIndicatorBuilder:
                              (context, url, downloadProgress) => Center(
                                  child: CircularProgressIndicator(
                                      value: downloadProgress.progress,
                                      color: Colors.black12)),
                          errorWidget: (context, url, error) => Icon(
                            Icons.error,
                            color: Colors.red,
                          ),
                        )),
                  );
                }).toList(),
              );
            }),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 50.h,
            alignment: Alignment.center,
            margin: EdgeInsets.only(bottom: 30.h),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemBuilder: (c, i) {
                // bool isActive= currentIndex == i;
                return GetBuilder<detailsController>(builder: (c) {
                  return dotIndicator(currentIndex: currentIndex, i: i);
                });
              },
              itemCount: model.imageUrlsList.length,
            ),
          ),
        )
      ],
    ),
    alwaysShowLeadingAndAction: true,
    actions: [
      Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.all(8),
        child: GetBuilder<itemsController>(builder: (c) {
          bool isLikeed = false;
          for (estateModel i in c.favoriteList.value) {
            if (i.objId == model.objId) {
              isLikeed = true;
            }
          }
          return LikeButton(
            isLiked: isLikeed,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            animationDuration: Duration(seconds: 1),
            circleColor: CircleColor(start: Colors.white, end: Colors.red),
            bubblesColor: const BubblesColor(
              dotPrimaryColor: Colors.white,
              dotSecondaryColor: Colors.red,
            ),
            likeBuilder: (bool isLiked) {
              return SvgPicture.asset(
                "assets/svgs/heart.svg",
                color: isLiked ? Colors.red : Colors.white,
              );
            },
            onTap: (isLiked) async {
              bool fav = !isLiked;
              if (fav) {
                if (!c.favoriteList.value.contains(model.objId)) {
                  c.addToFav(objId: model.objId);
                }
              } else {
                c.removeFromFav(objId: model.objId);
              }
              return !isLiked;
            },
          );
        }),
      ),
    ],
    leading: Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.5),
        border: Border.all(color: HexColor("D9D9D9"), width: 1),
        borderRadius: BorderRadius.circular(11.r),
      ),
      child: IconButton(
        onPressed: () {
          Get.back();
        },
        icon: Icon(
          Icons.arrow_back,
          color: HexColor("4E4E4E"),
        ),
      ),
    ),
    bottomNavigationBar: Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
      child: Stack(
        alignment: Alignment.center,
        clipBehavior: Clip.hardEdge,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Visibility(
                visible: model.comm_call,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: CircleBorder(
                            side: BorderSide(
                                color: HexColor("0C5880"), width: 1))),
                    onPressed: () {
                      makeCall(comm: model.comm);
                    },
                    child: CircleAvatar(
                      radius: 30.r,
                      backgroundColor: HexColor("FFFFFF"),
                      child: Icon(
                        Icons.call,
                        color: HexColor("0C5880"),
                        // size: 35.sp,
                      ),
                    )),
              ),
              SizedBox(
                width: 20.w,
              ),
              Visibility(
                visible: model.comm_wh,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: const CircleBorder(
                            side: BorderSide(color: Colors.green, width: 1))),
                    onPressed: () async {
                      await _launchWH(comm: model.comm);
                    },
                    child: CircleAvatar(
                      child: Icon(
                        Icons.whatsapp_rounded,
                        color: Colors.green,
                        // size: 35.sp,
                      ),
                      radius: 30.r,
                      backgroundColor: HexColor("FFFFFF"),
                    )),
              ),
              SizedBox(
                width: 20.w,
              ),
              Visibility(
                visible: model.comm_tele,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: CircleBorder(
                          side: BorderSide(color: Colors.blue, width: 1)),
                    ),
                    onPressed: () async {
                      await _launchTG(userName: model.userName);
                    },
                    child: CircleAvatar(
                      radius: 30.r,
                      backgroundColor: HexColor("FFFFFF"),
                      child: const Icon(
                        Icons.telegram,
                        color: Colors.blue,
                        // size: 35.sp,
                      ),
                    )),
              ),
            ],
          ),
          SizedBox(
            width: double.infinity,
            child: Wrap(
              direction: Axis.vertical,
              textDirection: TextDirection.rtl,
              spacing: 10.h,
              children: [
                Text(
                  "للتواصل",
                  style: commStyle,
                  textAlign: TextAlign.center,
                ),
                Text(
                  model.comm,
                  style: commStyle,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          )
        ],
      ),
    ),
    stretchTriggerOffset: 120.h,
    fullyStretchable: true,
    expandedBody: Stack(
      // fit: StackFit.expand,
      // alignment: Alignment.center,
      children: [
        SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: FittedBox(
            fit: BoxFit.cover,
            child: Blur(
              blur: 7,
              child: bg,
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: GetBuilder<detailsController>(builder: (c) {
            PageController pv = PageController(initialPage: currentIndex);
            return PageView.builder(
                onPageChanged: (i) {
                  currentIndex = i;
                  c.update();
                },
                controller: pv,
                itemCount: model.imageUrlsList.length,
                pageSnapping: true,
                physics: const BouncingScrollPhysics(),
                itemBuilder: (context, pagePosition) {
                  return CachedNetworkImage(
                    imageUrl: model.imageUrlsList[pagePosition],
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    progressIndicatorBuilder:
                        (context, url, downloadProgress) => Center(
                            child: CircularProgressIndicator(
                                value: downloadProgress.progress,
                                color: Colors.black12)),
                    errorWidget: (context, url, error) => Icon(
                      Icons.error,
                      color: Colors.red,
                    ),
                  );
                });
            // return CarouselSlider(
            //   options: CarouselOptions(
            //     // aspectRatio: 4 / 3,
            //       enlargeCenterPage: true,
            //       scrollPhysics: BouncingScrollPhysics(),
            //       enableInfiniteScroll: false,
            //       onPageChanged: (i, r) {
            //         currentIndex = i;
            //         c.update();
            //       },
            //       initialPage: currentIndex),
            //   items: model.imageUrlsList.map((i) {
            //     return SizedBox.expand(
            //       child: CachedNetworkImage(
            //         imageUrl: i,
            //         imageBuilder: (context, imageProvider) => Container(
            //           decoration: BoxDecoration(
            //             image: DecorationImage(
            //               image: imageProvider,
            //               fit: BoxFit.contain,
            //             ),
            //           ),
            //         ),
            //         progressIndicatorBuilder:
            //             (context, url, downloadProgress) => Center(
            //             child: CircularProgressIndicator(
            //                 value: downloadProgress.progress,
            //                 color: Colors.black12)),
            //         errorWidget: (context, url, error) => Icon(
            //           Icons.error,
            //           color: Colors.red,
            //         ),
            //       ),
            //     );
            //   }).toList(),
            // );
          }),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 50.h,
            alignment: Alignment.center,
            margin: EdgeInsets.only(bottom: 30.h),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemBuilder: (c, i) {
                // bool isActive= currentIndex == i;
                return GetBuilder<detailsController>(builder: (c) {
                  return dotIndicator(currentIndex: currentIndex, i: i);
                });
              },
              itemCount: model.imageUrlsList.length,
            ),
          ),
        )
      ],
    ),
    body: [
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 12.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 8.h),
              decoration: BoxDecoration(
                  color: HexColor("EDF1FD"),
                  borderRadius: BorderRadius.circular(8.r)),
              child: Text(
                model.state,
                style: TextStyle(
                    fontFamily: "assets/fonts/bungee_hairline.ttf",
                    fontSize: 16.sp,
                    color: HexColor("0D4599")),
                // style: GoogleFonts.bungeeHairline(
                //     fontSize: 16.sp, color: HexColor("0D4599")),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 14.h),
              child: Text(
                model.name,
                style: TextStyle(
                    fontFamily: "assets/fonts/bungee_hairline.ttf",
                    fontSize: 26.sp,
                    color: HexColor("0D4599")),
                // style: GoogleFonts.bungeeHairline(
                //     fontSize: 24.sp, color: HexColor("0D4599")),
              ),
            ),
          ],
        ),
      ),
      Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.w),
                child: Text(
                  "${model.updatedAt.year}-${model.updatedAt.month}-${model.updatedAt.day}",
                  style: TextStyle(color: HexColor("6A6A6A"), fontSize: 16.sp),
                ),
              ),
              Row(
                children: [
                  Text(
                    model.location,
                    style: locationStyle,
                  ),
                  Text(
                    " - ",
                    style: locationStyle,
                  ),
                  Text(
                    model.city,
                    style: locationStyle,
                  ),
                  SizedBox(
                    width: 4.w,
                  ),
                  Icon(
                    Icons.location_on_rounded,
                    color: HexColor("0D4599"),
                    size: 22.sp,
                  ),
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            textDirection: TextDirection.rtl,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                textDirection: TextDirection.rtl,
                children: [
                  Icon(
                    Icons.square_foot_outlined,
                    color: HexColor("9B9B9B"),
                    size: 22.sp,
                  ),
                  SizedBox(
                    width: 2.w,
                  ),
                  Text(
                    model.space,
                    style: attrStyle,
                    textDirection: TextDirection.rtl,
                  ),
                ],
              ),
              SizedBox(
                width: 25.w,
              ),
              Row(
                textDirection: TextDirection.rtl,
                children: [
                  Icon(
                    Icons.bed_outlined,
                    color: HexColor("9B9B9B"),
                    size: 22.sp,
                  ),
                  SizedBox(
                    width: 2.w,
                  ),
                  Text(
                    model.rooms_count,
                    style: attrStyle,
                  )
                ],
              ),
              SizedBox(
                width: 25.w,
              ),
              Row(
                textDirection: TextDirection.rtl,
                children: [
                  Icon(
                    Icons.bathroom_outlined,
                    color: HexColor("9B9B9B"),
                    size: 22.sp,
                  ),
                  SizedBox(
                    width: 2.w,
                  ),
                  Text(
                    model.baths_count,
                    style: attrStyle,
                  )
                ],
              )
            ],
          ),
          SizedBox(
            height: 8.h,
          ),
          Padding(
            padding: EdgeInsets.only(right: 12.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  "الوصف",
                  style: TextStyle(
                      fontFamily: "assets/fonts/bungee_hairline.ttf",
                      fontSize: 22.sp,
                      color: HexColor("0D4599")),
                  // style: GoogleFonts.bungeeHairline(
                  //     fontSize: 22.sp, color: HexColor("0D4599")),
                  textAlign: TextAlign.start,
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(vertical: 8.h, horizontal: 15.w),
            padding: EdgeInsets.symmetric(vertical: 13.h, horizontal: 18.w),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(24.r),
                color: HexColor("EDF1FD")),
            child: Text(
              model.desc,
              style: TextStyle(
                  fontFamily: "assets/fonts/jost_light.ttf",
                  fontSize: 18.sp,
                  color: HexColor("3F3F3F")),
              // style:
              //     GoogleFonts.jost(fontSize: 18.sp, color: HexColor("3F3F3F")),
              textAlign: TextAlign.start,
              textDirection: TextDirection.rtl,
            ),
          ),
        ],
      ),
    ],
  );
}

Widget bodyMobile_1(
    {required BuildContext context,
    required estateModel model,
    required CachedNetworkImage bg}) {
  int currentIndex = 0;
  final commStyle = TextStyle(
      fontFamily: "assets/fonts/bungee_hairline.ttf",
      fontSize: 20.sp,
      color: HexColor("0D4599"));

  final locationStyle = TextStyle(
      fontFamily: "assets/fonts/bungee_hairline.ttf",
      fontSize: 16.sp,
      color: HexColor("405468"));
  final attrStyle = TextStyle(
      fontFamily: "assets/fonts/jost_light.tff",
      fontSize: 15.sp,
      color: HexColor("5C5C5C"));
  return DraggableHome(
    headerExpandedHeight: 0.5,
    stretchMaxHeight: 0.9,
    title: Container(),
    appBarColor: Colors.white,
    backgroundColor: Colors.white,
    headerWidget: Stack(
      // fit: StackFit.expand,
      // alignment: Alignment.center,
      children: [
        SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: FittedBox(
            fit: BoxFit.cover,
            child: Blur(
              blur: 7,
              child: bg,
            ),
          ),
        ),
        SizedBox.expand(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 35.h),
            child: GetBuilder<detailsController>(builder: (c) {
              return CarouselSlider(
                options: CarouselOptions(
                    // aspectRatio: 4 / 1,
                    enlargeCenterPage: true,
                    enableInfiniteScroll: false,
                    onPageChanged: (i, r) {
                      currentIndex = i;
                      c.update();
                    },
                    initialPage: currentIndex),
                items: model.imageUrlsList.map((i) {
                  return SizedBox.expand(
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(45.r),
                        child: CachedNetworkImage(
                          imageUrl: i,
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          progressIndicatorBuilder:
                              (context, url, downloadProgress) => Center(
                                  child: CircularProgressIndicator(
                                      value: downloadProgress.progress,
                                      color: Colors.black12)),
                          errorWidget: (context, url, error) => Icon(
                            Icons.error,
                            color: Colors.red,
                          ),
                        )),
                  );
                }).toList(),
              );
            }),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 50.h,
            alignment: Alignment.center,
            margin: EdgeInsets.only(bottom: 30.h),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemBuilder: (c, i) {
                // bool isActive= currentIndex == i;
                return GetBuilder<detailsController>(builder: (c) {
                  return dotIndicator(currentIndex: currentIndex, i: i);
                });
              },
              itemCount: model.imageUrlsList.length,
            ),
          ),
        )
      ],
    ),
    alwaysShowLeadingAndAction: true,
    actions: [
      Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.all(8),
        child: GetBuilder<itemsController>(builder: (c) {
          bool isLikeed = false;

          for (estateModel i in c.favoriteList.value) {
            if (i.objId == model.objId) {
              isLikeed = true;
            }
          }
          return LikeButton(
            isLiked: isLikeed,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            animationDuration: Duration(seconds: 1),
            circleColor: CircleColor(start: Colors.white, end: Colors.red),
            bubblesColor: const BubblesColor(
              dotPrimaryColor: Colors.white,
              dotSecondaryColor: Colors.red,
            ),
            likeBuilder: (bool isLiked) {
              return SvgPicture.asset(
                "assets/svgs/heart.svg",
                color: isLiked ? Colors.red : Colors.white,
              );
            },
            onTap: (isLiked) async {
              bool fav = !isLiked;
              if (fav) {
                if (!c.favoriteList.value.contains(model.objId)) {
                  c.addToFav(objId: model.objId);
                }
              } else {
                c.removeFromFav(objId: model.objId);
              }
              return !isLiked;
            },
          );
        }),
      ),
    ],
    leading: Container(
      margin: const EdgeInsets.all(8),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border.all(color: HexColor("D9D9D9"), width: 1),
        borderRadius: BorderRadius.circular(11.r),
      ),
      child: IconButton(
        onPressed: () {
          Get.back();
        },
        icon: Icon(
          Icons.arrow_back,
          color: HexColor("4E4E4E"),
        ),
      ),
    ),
    bottomNavigationBar: Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 4.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            flex: 20,
            child: Wrap(
              alignment: WrapAlignment.start,
              direction: Axis.horizontal,
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 10.w,
              children: [
                SizedBox(width: 2.w,),
                Visibility(
                  visible: model.comm_tele,
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black54.withOpacity(0.2),
                          blurRadius: 8.r,
                          offset: Offset.zero
                        )
                      ]
                    ),
                    child: IconButton(
                      padding: EdgeInsets.zero,
                      iconSize: 45.sp,
                      onPressed: () async {
                        await _launchTG(userName: model.userName);
                      },
                      icon: Icon(
                        Icons.telegram,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                  // child: ElevatedButton(
                  //   style: ButtonStyle(
                  //       tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  //       padding: MaterialStateProperty.all(EdgeInsets.symmetric(horizontal: 30.w)),
                  //       minimumSize:
                  //       MaterialStateProperty.all(Size(0.w, 0.h)),
                  //       elevation: MaterialStateProperty.all(2),
                  //       backgroundColor: MaterialStateProperty.all(
                  //           Colors.white.withOpacity(0.9)),
                  //       shape: MaterialStateProperty.all(
                  //           CircleBorder(side: BorderSide(width: 1,color: Colors.blue)))),
                  //     // style: ElevatedButton.styleFrom(
                  //     //   tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  //     //   padding: EdgeInsets.zero,
                  //     //   shape: const CircleBorder(
                  //     //       side: BorderSide(color: Colors.blue, width: 1)),
                  //     // ),
                  //     onPressed: () async {
                  //       // await _launchTG(userName:model.userName);
                  //     },
                  //     child: const Icon(
                  //       Icons.telegram,
                  //       color: Colors.blue,
                  //       // size: 35.sp,
                  //     )),
                ),
                Visibility(
                  visible: model.comm_wh,
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black54.withOpacity(0.2),
                              blurRadius: 8.r,
                              offset: Offset.zero
                          )
                        ]
                    ),
                    child: IconButton(
                      padding: EdgeInsets.zero,
                      iconSize: 45.sp,
                      onPressed: () async {
                        await _launchWH(comm: model.comm);
                      },
                      icon: Icon(
                        Icons.whatsapp_rounded,
                        color: Colors.green,
                      ),
                    ),
                  ),

                ),
                Visibility(
                  visible: model.comm_call,
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black54.withOpacity(0.2),
                              blurRadius: 8.r,
                              offset: Offset.zero
                          )
                        ]
                    ),
                    child: IconButton(
                      padding: EdgeInsets.zero,
                      iconSize: 40.sp,
                      onPressed: () {
                        makeCall(comm: model.comm);
                      },
                      icon: Icon(
                        Icons.call,
                        color: HexColor("0C5880"),
                      ),
                    ),
                  ),

                  // child: IconButton(
                  //   onPressed: () {
                  //     makeCall(comm: model.comm);
                  //   },
                  //   icon: Icon(
                  //     Icons.call,
                  //     color: HexColor("0C5880"),
                  //   ),
                  //   // iconSize: 40.sp,
                  // ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 5,
            child: Wrap(
              direction: Axis.vertical,
              textDirection: TextDirection.rtl,
              spacing: 10.h,
              children: [
                Text(
                  "للتواصل",
                  style: commStyle,
                  // style: GoogleFonts.bungeeHairline(
                  //     fontSize: 20.sp, color: HexColor("0D4599")),
                  textAlign: TextAlign.center,
                ),
                Text(
                  model.comm,
                  style: commStyle,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          )
        ],
      ),
    ),
    stretchTriggerOffset: 120.h,
    fullyStretchable: true,
    expandedBody: Stack(
      // fit: StackFit.expand,
      children: [
        SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: FittedBox(
            fit: BoxFit.cover,
            child: Blur(
              blur: 7,
              child: bg,
            ),
          ),
        ),
        SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: GetBuilder<detailsController>(builder: (c) {
            PageController pv = PageController(initialPage: currentIndex);
            return PageView.builder(
                onPageChanged: (i) {
                  currentIndex = i;
                  c.update();
                },
                controller: pv,
                itemCount: model.imageUrlsList.length,
                pageSnapping: true,
                physics: const BouncingScrollPhysics(),
                itemBuilder: (context, pagePosition) {
                  return CachedNetworkImage(
                    imageUrl: model.imageUrlsList[pagePosition],
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    progressIndicatorBuilder:
                        (context, url, downloadProgress) => Center(
                            child: CircularProgressIndicator(
                                value: downloadProgress.progress,
                                color: Colors.black12)),
                    errorWidget: (context, url, error) => Icon(
                      Icons.error,
                      color: Colors.red,
                    ),
                  );
                });
          }),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 50.h,
            alignment: Alignment.center,
            margin: EdgeInsets.only(bottom: 80.h),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemBuilder: (c, i) {
                // bool isActive= currentIndex == i;
                return GetBuilder<detailsController>(builder: (c) {
                  return dotIndicator(currentIndex: currentIndex, i: i);
                });
              },
              itemCount: model.imageUrlsList.length,
            ),
          ),
        )
        // Center(
        //   child: Container(
        //     color: Colors.yellow,
        //     child: ListView.builder(
        //       scrollDirection: Axis.horizontal,
        //     itemCount: 10,
        //     itemBuilder: (c, i) {
        //         bool isActive= currentIndex==i;
        //       return SizedBox(
        //         height: 10,
        //         child: AnimatedContainer(
        //           duration: Duration(milliseconds: 150),
        //           margin: EdgeInsets.symmetric(horizontal: 4.0),
        //           height: isActive
        //               ? 10:8.0,
        //           width: isActive
        //               ? 12:8.0,
        //           decoration: BoxDecoration(
        //             boxShadow: [
        //               isActive
        //                   ? BoxShadow(
        //                 color: Color(0XFF2FB7B2).withOpacity(0.72),
        //                 blurRadius: 4.0,
        //                 spreadRadius: 1.0,
        //                 offset: Offset(
        //                   0.0,
        //                   0.0,
        //                 ),
        //               )
        //                   : BoxShadow(
        //                 color: Colors.transparent,
        //               )
        //             ],
        //             shape: BoxShape.circle,
        //             color: isActive ? Color(0XFF6BC4C9) : Color(0XFFEAEAEA),
        //           ),
        //         ),
        //       );;
        //     }),
        //   ),
        // )
        // SizedBox(
        //   width: double.infinity,
        //   height: double.infinity,
        //   // color: Colors.white,
        //   child: GetBuilder<detailsController>(builder: (c) {
        //     return CarouselSlider(
        //       options: CarouselOptions(
        //           // aspectRatio: 4 / 3,
        //           enlargeCenterPage: true,
        //           scrollPhysics: BouncingScrollPhysics(),
        //           enableInfiniteScroll: false,
        //           onPageChanged: (i, r) {
        //             currentIndex = i;
        //             c.update();
        //           },
        //           initialPage: currentIndex),
        //       items: model.imageUrlsList.map((i) {
        //         return SizedBox.expand(
        //           child: CachedNetworkImage(
        //             imageUrl: i,
        //             imageBuilder: (context, imageProvider) => Container(
        //               decoration: BoxDecoration(
        //                 image: DecorationImage(
        //                   image: imageProvider,
        //                   fit: BoxFit.contain,
        //                 ),
        //               ),
        //             ),
        //             progressIndicatorBuilder:
        //                 (context, url, downloadProgress) => Center(
        //                     child: CircularProgressIndicator(
        //                         value: downloadProgress.progress,
        //                         color: Colors.black12)),
        //             errorWidget: (context, url, error) => Icon(
        //               Icons.error,
        //               color: Colors.red,
        //             ),
        //           ),
        //         );
        //       }).toList(),
        //     );
        //   }),
        // ),
      ],
    ),
    // expandedBody: Stack(
    //   fit: StackFit.expand,
    //   children: [
    //     SizedBox(
    //       width: double.infinity,
    //       height: double.infinity,
    //       child: FittedBox(
    //         fit: BoxFit.cover,
    //         child: Blur(
    //           blur: 7,
    //           child: bg,
    //         ),
    //       ),
    //     ),
    //     PageView.builder(
    //         itemCount: model.imageUrlsList.length,
    //         pageSnapping: true,
    //         itemBuilder: (context,pagePosition){
    //           return Container(
    //               margin: EdgeInsets.all(10),
    //               child: Image.network(model.imageUrlsList[pagePosition]));
    //         })
    //   ],
    // ),
    body: [
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 12.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 8.h),
              decoration: BoxDecoration(
                  color: HexColor("EDF1FD"),
                  borderRadius: BorderRadius.circular(8.r)),
              child: Text(
                model.state,
                style: TextStyle(
                    fontFamily: "assets/fonts/bungee_hairline.ttf",
                    fontSize: 16.sp,
                    color: HexColor("0D4599")),
                // style: GoogleFonts.bungeeHairline(
                //     fontSize: 16.sp, color: HexColor("0D4599")),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 14.h),
              child: Text(
                model.name,
                style: TextStyle(
                    fontFamily: "assets/fonts/bungee_hairline.ttf",
                    fontSize: 26.sp,
                    color: HexColor("0D4599")),
                // style: GoogleFonts.bungeeHairline(
                //     fontSize: 24.sp, color: HexColor("0D4599")),
              ),
            ),
          ],
        ),
      ),
      Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.w),
                child: Text(
                  "${model.updatedAt.year}-${model.updatedAt.month}-${model.updatedAt.day}",
                  style: TextStyle(color: HexColor("6A6A6A"), fontSize: 14.sp),
                ),
              ),
              Row(
                children: [
                  Text(
                    model.location,
                    style: locationStyle,
                  ),
                  Text(
                    " - ",
                    style: locationStyle,
                  ),
                  Text(
                    model.city,
                    style: locationStyle,
                  ),
                  SizedBox(
                    width: 4.w,
                  ),
                  Icon(
                    Icons.location_on_rounded,
                    color: HexColor("0D4599"),
                    size: 22.sp,
                  )
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            textDirection: TextDirection.rtl,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                textDirection: TextDirection.rtl,
                children: [
                  Icon(
                    Icons.square_foot_outlined,
                    color: HexColor("9B9B9B"),
                    size: 22.sp,
                  ),
                  SizedBox(
                    width: 2.w,
                  ),
                  Text(
                    model.space,
                    style: attrStyle,
                    textDirection: TextDirection.rtl,
                  ),
                ],
              ),
              SizedBox(
                width: 25.w,
              ),
              Row(
                textDirection: TextDirection.rtl,
                children: [
                  Icon(
                    Icons.bed_outlined,
                    color: HexColor("9B9B9B"),
                    size: 22.sp,
                  ),
                  SizedBox(
                    width: 2.w,
                  ),
                  Text(
                    model.rooms_count,
                    style: attrStyle,
                  )
                ],
              ),
              SizedBox(
                width: 25.w,
              ),
              Row(
                textDirection: TextDirection.rtl,
                children: [
                  Icon(
                    Icons.bathroom_outlined,
                    color: HexColor("9B9B9B"),
                    size: 22.sp,
                  ),
                  SizedBox(
                    width: 2.w,
                  ),
                  Text(
                    model.baths_count,
                    style: attrStyle,
                  )
                ],
              )
            ],
          ),
          SizedBox(
            height: 8.h,
          ),
          Padding(
            padding: EdgeInsets.only(right: 12.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  "الوصف",
                  style: TextStyle(
                      fontFamily: "assets/fonts/bungee_hairline.ttf",
                      fontSize: 22.sp,
                      color: HexColor("0D4599")),
                  // style: GoogleFonts.bungeeHairline(
                  //     fontSize: 22.sp, color: HexColor("0D4599")),
                  textAlign: TextAlign.start,
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(vertical: 8.h, horizontal: 15.w),
            padding: EdgeInsets.symmetric(vertical: 13.h, horizontal: 18.w),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(24.r),
                color: HexColor("EDF1FD")),
            child: Text(
              model.desc,
              style: TextStyle(
                  fontFamily: "assets/fonts/jost_light.ttf",
                  fontSize: 18.sp,
                  color: HexColor("3F3F3F")),
              // style:
              //     GoogleFonts.jost(fontSize: 18.sp, color: HexColor("3F3F3F")),
              textAlign: TextAlign.start,
              textDirection: TextDirection.rtl,
            ),
          ),
        ],
      ),
    ],
  );
}

void makeCall({required String comm}) {
  if ((!Platform.isWindows) && (!Platform.isMacOS)) {
    try {
      launchUrlString("tel:$comm");
    } catch (e) {
      showErrorSnack();
    }
  }
}

Future _launchTG({String? userName}) async {
  try {
    String url = Telegram.getLink(
      username: userName!,
    );
    await launch(url);
  } catch (e) {
    showErrorSnack();
  }
}

void showErrorSnack() {
  Get.snackbar("خطأ", "حدث خطأ ما",
      barBlur: 30,
      margin: EdgeInsets.only(bottom: 16.h),
      snackPosition: SnackPosition.BOTTOM,
      overlayColor: Colors.lightBlue,
      messageText: Text(
        " حدث خطأ ما",
        style: TextStyle(fontSize: 16.sp, color: Colors.black45),
        textAlign: TextAlign.start,
        textDirection: TextDirection.rtl,
      ),
      colorText: Colors.white,
      titleText: Text(
        "عذرآ,",
        style: TextStyle(fontSize: 20.sp, color: Colors.black),
        textAlign: TextAlign.start,
        textDirection: TextDirection.rtl,
      ));
}

Future _launchWH({required String comm}) async {
  try {
    comm = "${comm.substring(0, 0)}${comm.substring(1)}";
    String url = "https://wa.me/+963$comm";
    await launch(url);
  } catch (e) {
    Get.snackbar("خطأ", "حدث خطأ ما",
        barBlur: 30,
        margin: EdgeInsets.only(bottom: 16.h),
        snackPosition: SnackPosition.BOTTOM,
        overlayColor: Colors.lightBlue,
        messageText: Text(
          "حدث خطأ ما",
          style: TextStyle(fontSize: 16.sp, color: Colors.black45),
          textAlign: TextAlign.start,
          textDirection: TextDirection.rtl,
        ),
        colorText: Colors.white,
        titleText: Text(
          "عذرآ,",
          style: TextStyle(fontSize: 20.sp, color: Colors.black),
          textAlign: TextAlign.start,
          textDirection: TextDirection.rtl,
        ));
  }
}

Widget dotIndicator({required int currentIndex, required int i}) {
  return SizedBox(
    height: 10.h,
    child: AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 4.w),
      height: currentIndex == i ? 10.h : 8.h,
      width: currentIndex == i ? 12.w : 8.w,
      decoration: BoxDecoration(
        boxShadow: [
          currentIndex == i
              ? BoxShadow(
                  color: Colors.white.withOpacity(0.72),
                  blurRadius: 4.r,
                  spreadRadius: 1.r,
                  offset: Offset(
                    0.0,
                    0.0,
                  ),
                )
              : BoxShadow(
                  color: Colors.transparent,
                )
        ],
        shape: BoxShape.circle,
        color: currentIndex == i ? Colors.white : Colors.white54,
      ),
    ),
  );
}
