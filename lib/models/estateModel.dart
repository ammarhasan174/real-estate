import 'package:flutter/foundation.dart';

class estateModel {
  String name;
  String desc;
  String city;
  String state;
  String location;
  String space;
  String comm;
  List<String> imageUrlsList;
  bool comm_call;
  bool comm_wh;
  bool comm_tele;
  bool isSpecial;
  String rooms_count;
  String baths_count;
  String? userName;
  String objId;
  DateTime updatedAt;
  estateModel({
    required this.name,
    required this.desc,
    required this.city,
    required this.state,
    required this.location,
    required this.space,
    required this.comm,
    required this.imageUrlsList,
    required this.comm_call,
    required this.comm_wh,
    required this.comm_tele,
    required this.rooms_count,
    required this.baths_count,
    required this.objId,
    required this.updatedAt,
    required this.isSpecial,
    this.userName
  });
}
