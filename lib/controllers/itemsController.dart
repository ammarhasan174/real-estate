import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:parse_server_sdk/parse_server_sdk.dart';
import 'package:realestate/constants.dart';
import 'package:realestate/main.dart';
import 'package:realestate/models/estateModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class itemsController extends GetxController {
  List<estateModel> displayList = [];

  // List<estateModel> tempList = [];
  List<estateModel> mainList = [];
  RxList favoriteList = [].obs;
  TextEditingController searchCont = TextEditingController();
  FocusNode searchFoc = FocusNode();
  bool isSearching = false;
  bool showSearchBar = true;
  bool noResult = false;

  RxBool showLoading_load=false.obs;
  late SharedPreferences sp;

  RxString option_checked = "الكل".obs;

  @override
  void onInit() async {
    sp = await SharedPreferences.getInstance();
    super.onInit();
    await getData();
  }

  Future retriveFavItems() async {
    favoriteList.value.clear();
    List<String>? favObjList = sp.getStringList("fav");
    if (favObjList != null) {
      for(estateModel item in mainList){
        for(String objId in favObjList){
          if(item.objId == objId){
            favoriteList.value.add(item);
          }
        }
      }
    } else {
      print("favObj list is null");
    }
    for(var i in favoriteList.value){
      print("fav is $i");
    }
    // favoriteList.value=sortList(list: favoriteList.value as List<estateModel>);
    update();
  }
  Future removeFromFav({required String objId})async{
    print("removing $objId from favorite");
    List<String>? favObjList =sp.getStringList("fav");
    if(favObjList!=null){
      favObjList.removeWhere((element) => element==objId);
      await sp.setStringList("fav", favObjList);
    }
    await retriveFavItems();
    update();
  }

  Future addToFav({required String objId})async{
    print("adding $objId to favorite");
    List<String>? favObjList =sp.getStringList("fav");
    if(favObjList!=null){
      favObjList.add(objId);
      await sp.setStringList("fav", favObjList);
    }else{
      favObjList=[]..add(objId);
      await sp.setStringList("fav", favObjList);
    }
    await retriveFavItems();

    update();
  }

  Future getData() async {
    print("getting data....");
    mainList.clear();
    displayList.clear();
    showLoading_load.value=true;
    try{
      QueryBuilder<ParseObject> query =
      QueryBuilder<ParseObject>(ParseObject('estates'));
      final ParseResponse apiResponse = await query.query();
      if (apiResponse.success && apiResponse.results != null) {
        for (var item in apiResponse.result) {
          print(item["name"]);
          if(!item["ishidden"]){
            String name = item["name"];
            String desc = item["desc"];
            String city = item["city"];
            String state = item["state"];
            String location = item["location"];
            String space = item["space"];
            String comm = item["comm"];
            bool comm_call = item["comm_call"];
            bool comm_tele = item["comm_tele"];
            String? userName;
            if (comm_tele) {
              userName = item["tele_username"];
            }
            bool comm_wh = item["comm_wh"];
            bool isSpecial = item["isspecial"];
            String rooms_count = item["rooms_count"];
            String baths_count = item["baths_count"];
            String objId = item["objectId"];
            DateTime dt=item["updatedAt"];
            List<String> imageUrlList = [];
            for (var i = 1; i <= 10; i++) {
              try {
                String? imageUrl = item["image_$i"]["url"];
                if (imageUrl != null) {
                  imageUrlList.add(imageUrl);
                }
              } catch (e) {}
            }
            estateModel mModel = estateModel(
                name: name,
                desc: desc,
                city: city,
                state: state,
                location: location,
                space: space,
                comm: comm,
                imageUrlsList: imageUrlList,
                comm_call: comm_call,
                comm_wh: comm_wh,
                comm_tele: comm_tele,
                rooms_count: rooms_count,
                baths_count: baths_count,
                objId: objId,
                updatedAt: dt,
                userName: userName,
                isSpecial: isSpecial
            );
            displayList.add(mModel);
            mainList.add(mModel);
            await retriveFavItems();
          }
        }
        displayList=sortList(list: displayList);
        if(displayList.isEmpty){
          noResult=true;
        }
        showLoading_load.value=false;
        update();
      }else{
        showLoading_load.value=false;
      }
    }catch(e){
      showLoading_load.value=false;
    }
  }
  List<estateModel> sortList({required List<estateModel> list}){
    list.sort((a,b)=>a.updatedAt.compareTo(b.updatedAt));

    list.sort((a,b){
      if(!a.isSpecial){
        return 1;
      }
      return -1;
    });
    return list;
  }
  void filterList() {
    String str = searchCont.text;
    displayList.clear();
    displayList.addAll(mainList);
    if (option_checked != "الكل") {
      displayList.retainWhere((element) => element.state == option_checked.value);
    } else {
      // displayList.addAll(mainList);
    }
    if (str.isNotEmpty) {
      displayList.retainWhere((element) =>
          element.name.contains(str) || element.desc.contains(str) || element.state.contains(str));
    } else {
      print("str is Empty");
    }
    if (displayList.isEmpty) {
      noResult = true;
    }
    displayList=sortList(list: displayList);
    update();
  }
// void searchFunction({required String str}) {
//   tempList.clear();
//   if (str.isNotEmpty) {
//     isSearching = true;
//     update();
//     for (estateModel i in displayList) {
//       if (i.name.contains(str) || i.desc.contains(str)) {
//         tempList.add(i);
//       }
//     }
//     update();
//   } else {
//     isSearching = false;
//     update();
//   }
// }
}
